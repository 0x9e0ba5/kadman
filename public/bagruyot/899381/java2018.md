
# פתרון בגרות 2018 בשפת גאוו'ה

## שאלה 1

```java
public int lastOddValue() {
	int lastOdd = this.arrayNum[0];
	
	for (int i =1; i<this.arrayNum.length;i++) {
		if ((this.arrayNum[i] % 2) != 0)
			lastOdd=this.arrayNum[i];
	}
	
	return lastOdd;
}
```

## שאלה 2

```java
public static void theWinner(Vote[] arr) {
        int[] songs = new int[41];

        for(int i = 0; i < songs.length; i++)
            songs[i] = 0;

        for(int j = 0; j < arr.length; j++) {
            songs[arr[j].getFirst()] += 7;
            songs[arr[j].getSecond()] += 5;
            songs[arr[j].getThird()] += 1;
        }

        int maxP = 0;

        for(int i = 0; i < songs.length; i++) {
            if(songs[i] > songs[maxP]) {
                maxP = i;
            }
        }

        System.out.println(maxP);
}
```

## שאלה 3

#### סעיף א

```java
public Time(int hour,int minute) {
	if (hour <0 || hour>23)
		hour = 0;
	this.hour = hour;
	if (minute <0 || minute > 59)
		minute=0;
	this.minute = minute;
}
```

#### סעיף ב

```java
public class Flight {
	private String name;
	private String destination;
	private String flightCode;
	private Time flightTime;
	(...)
}
```

#### סעיף ג
1. False

2.	כאשר התנאי שבתוך הלולאה לא מתקיים,
	הפעולה תחזיר שקר מבלי להמשיך לבדוק את שאר אברי המערך.

3. 
```java
public boolean isFly() {
	for (int i = 0; i < this.flights.length; i++) {
		if (this.flights[i].getName().equals("Sky"))
			return true;
	}
	return false;
}
```

## שאלה 4

#### סעיף א
```java
public static int lastAndRemove(Stack<Integer> s) {
	Stack<Integer> temp = new Stack<Integer>();
	
	while (!s.isEmpty())
		temp.push(s.pop());
		
	int num = temp.pop();
	
	while (!temp.isEmpty())
		s.push(temp.pop());
		
	return num;
}
```
#### סעיף ב
```java
public static Stack<TwoItems> stackTwoItems(Stack<Integer> stk1) {
	int num1,num2;
	Stack<TwoItems> st = new Stack<TwoItems>();
	TwoItems t;
	
	while (!stk1.isEmpty()) {
		num1 = stk1.pop();
		num2 = lastAndRemove(stk1);
		t = new TwoItems(num1,num2);
		st.push(t);
	}
	
	return st;
}
```

## שאלה 5

#### סעיף א

1.
|   מצביע lst לאן			 | ch |  lst ==null    | lst.getValue() == ch | return 				 | lst 									 |
|:----------------------:|:--:|:--------------:|:--------------------:|:--------------------:|:-------------------------------------:|
|   'c'                  |'v' |     False      |       False          |sod1(lst.getNext(),ch)|[(**'c'**)-> ('d') -> ('v') -> ('h')]  |
|   'd'                  |'v' |     False      |       False          |sod1(lst.getNext(),ch)| [('c')-> (**'d'**) -> ('v') -> ('h')] |
|   'v'                  |'v' |     False      |       True           |lst                   | [('c')-> ('d') -> (**'v'**) -> ('h')] |

2. 	הפעולה מקבלת הפנייה לרשימה של תווים ותו נוסף,
	במידה התו מופיע ברשימה הפעולה מחזירה null. אחרת מחזירה הפנייה לתו ברשימה.
	
5.	O(n) סיבוכיות הפעולה היא.
	פעם אחת n נימוק: במקרה הגרוע ביותר בפעולה תעבור על כל איבר ברשימה שגודלה.

#### סעיף ב

הפעולה מקבל רשימה של תווים, במידה וברשימה מופיע התו `a` וגם התו `b`
הפעולה מחזירה אמת, אחרת הפעולה מחזירה שקר.

#### סעיף ג


## שאלה 6

#### סעיף א

```java
public static boolean treeLessThanTree(BinNode<Integer> t1, BinNode<Integer> t2) 
{
	if(t1 == null)
		return true;

	if(lessThanTree(t2, t1.getValue()))
		return false;

	return(treeLessThanTree(t2, t1.getLeft()) &&
			treeLessThanTree(t2, t1.getRight()));
}
```

#### סעיף ב

aa
<!--stackedit_data:
eyJoaXN0b3J5IjpbODgzOTYzNzAsMjEwMjQwNTY3MCwxMTY3OT
AwOTAwLDExNjc5MDA5MDAsLTE1ODQxNzc5NDgsLTY4MjUyOTY1
MCwtMjA5MTcxMDY5NSw2Mzc3MDk5NDksLTEyMzczNDUyNSwxNT
g4NjQzMDcyLC0xMjM3MzQ1MjUsODA1OTUyNjExLC0xODgxMTg1
MDc5LDc4ODY2OTc0OSwzNzkxMDQyNzEsNTk4MjI3ODM5XX0=
-->